static int g_sergent = -1;
static int g_sergent_follower1 = -1;
static int g_sergent_follower2 = -1;
static int g_sergent_follower3 = -1;

void Voice2(wchar_t *s, float f)
{
    call_2C(0xFA0, s);
    call_2C(0xFA2);
    call_2C(0xC8, f);
}

void RadioBegin()
{
	call_2C(0xFA0, L"MusenBegin");
}

void RadioEnd()
{
    call_2C(0xFA0, L"MusenEnd");
}

void RadioVoice(wchar_t *s, float f)
{
	call_2C(0xFA2);
	call_2C(0xC8, f);
	RadioBegin();
	call_2C(0xFA0, s);
	RadioEnd();
}

void EconomyMode(int i)
{
	call_2C(0x11, i);
}

void WaitAiMoveEnd(int i)
{

	while (true) 
	{
		if (call_2C(0xC1F, i)) return;
		call_2D(0x0);
	}
}

void InitializeCommon()
{
	call_2C(0x5);
}

void MissionClear_Common(float f)
{
	if (call_2D(0x2710)) call_2C(0x2713);
	int i = call_2C(0x27, L"app:/ui/lyt_HUiMissionCleared.sgo");
	call_2C(0xC8, f);
	int i2 = call_2C(0x1E, L"ui_fade_screen_simple");
	call_2C(0x32, i2, 3, 3.0f);
	call_2C(0x33, i2);
	call_2C(0x1F, i);
	call_2C(0x1F, i2);
	call_2C(0x3, 1);
}

void MissionClear()
{
	call_2C(0x12D, 2.0f);
	call_2C(0xC8, 1.5f);
	call_2C(0x34, 0);
	call_2C(0x12C, L"Jingle_MissionCleared");
	MissionClear_Common(6.0f);
}

void FinalMissionClear()
{
	call_2C(0x12D, 2.0f);
	call_2C(0xC8, 1.5f);
	call_2C(0x34, 0);
	call_2C(0x12C, L"Jingle_MissionClearedFinal");
	MissionClear_Common(10.0f);
}

void MissionEscapeClear()
{
	call_2C(0x12D, 2.0f);
	call_2C(0xC8, 1.5f);
	call_2C(0x34, 0);
	call_2C(0x12C, L"Jingle_MissionEscape");
	MissionClear_Common(7.0f);
}

void MissionGameOverEvent()
{
	int i = 0;
	if (call_2D(0x1, 2) == 0) { return; }
	call_2C(0x2711);
	call_2C(0xC8, 3.0f);
	call_2C(0x34, 0);
	call_2C(0x12D, 2.0f);
	call_2C(0xC8, 1.5f);
	if (call_2D(0x2710))
	{
		call_2C(0x2713);
	}
	int i2 = call_2C(0x27, L"app:/ui/lyt_HUiMissionFailed.sgo");
	call_2C(0x12C, L"Jingle_MissionFailed");
	call_2C(0xC8, 5.0f);
	call_2C(0x2712);
	if (call_2D(0x2710))
	{
		call_2C(0x2713);
	}
	int i3 = call_2C(0x27, L"app:/ui/lyt_HUiFailedResult.sgo");
	call_2C(0x21, i3);
	i = call_2C(0x26, L"", L"", 0);
	int i4 = call_2C(0x1E, L"ui_fade_screen_simple");
	call_2C(0x32, i4, 3, 0.5f);
	call_2C(0x33, i4);
	call_2C(0x1F, i2);
	call_2C(0x1F, i4);
	if (i == 0)
	{
		call_2C(0x3, 3);
		return;
	}
	if (i != 2) return;
	call_2C(0x3, 2);
}

void SceneEffect_Snow(float f, float f2, float f3, float f4)
{
	call_2C(0x13EC, 10.0f, 0.1f, 0.05f, f2, f, 0.5f, 0.5f, 0.5f, 2.0f, 2.0f, 2.0f, 100.0f, f3, 40.0f, f4);
}

void SceneEffect_Rain(float f, float f2, float f3, float f4, float f5, float f6)
{
	call_2C(0x13ED, f, f2, 0.5f, f3, 1.0f, 2.0f, 2.0f, 2.0f, 200.0f, f4, 40.0f, f5, f6);
}

void SceneEffect_RainEx(float f, float f2, float f3, float f4, float f5, float f6)
{
	call_2C(0x13ED, f, f2, 0.5f, f3, f4, 2.0f, 2.0f, 2.0f, 200.0f, f5, 40.0f, f6, f7);
}

void SceneEffect_FugitiveDust(float f, float f2, float f3, float f4, float f5, float f6)
{
	call_2C(0x13EE, f, f2, 10.0f, f3, f4, f5, f6);
}

void SceneEffect_Fog(float f, float f2, float f3, float f4, float f5, float f6)
{
	call_2C(0x13EF, f, f2, 10.0f, f3, f4, f5, f6);
}

void Main()
{
	EconomyMode(0);
	InitializeCommon();
	call_2C(0x0A);
	call_2C(0x0D, L"app:/ui/lyt_HUiMissionCleared.sgo", -1);
	call_2C(0x0D, L"app:/ui/lyt_HUiMissionFailed.sgo", -1);
	call_2C(0x0D, L"app:/ui/lyt_HUiFailedResult.sgo", -1);
	call_2C(0x0E, L"app:/Map/nw_Hillycity_light.mac", L"fine", -1);
	call_2C(0x10);
	call_2C(0xC);
	call_2C(0x2710);
	call_2C(0xB);
	call_2C(0x64, L"app:/Map/nw_Hillycity_light.mac", L"fine", -1);
	call_2C(0x3E8, L"プレイヤー");
}